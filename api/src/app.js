const express = require('express')
const app = express()
const routes = require('./routes')
const cors = require('cors')

require('./models')

app.use(cors())
app.use(express.json())

app.name = 'Marti Lab'

app.use('/', routes)

module.exports = { app }
