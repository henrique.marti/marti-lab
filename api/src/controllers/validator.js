const uuid = require('uuid')

const Validator = {
  errorList: [],
    
  addError: (errorMessage) => {
    errorList.push(errorMessage)
  },
  
  clearExistingErrors: () => {
    errorList = []
  },

  throwExistingErrors: () => {
    if (errorList.length == 0) {
      return
    }

    throw TypeError(errorList.join('<||>'))
  },

  validateTextFilter: (fieldName, fieldValue) => {
    return Validator.validateText('field', fieldName, fieldValue, false)
  },

  validateTextField: (fieldName, fieldValue, required = true) => {
    return Validator.validateText('field', fieldName, fieldValue, required)
  },

  validateText: (fieldLabel, fieldName, fieldValue, required = true) => {
    if (required && fieldValue === null) {
      Validator.addError(`The ${fieldLabel} "${fieldName}" was not sent`)
    }

    if (fieldValue !== null && fieldValue.trim() == '') {
      Validator.addError(`The value sent in the "${fieldName}" ${fieldLabel} is invalid`)
    }

    return fieldValue
  },

  validateIntegerFilter: (fieldName, fieldValue, minValue = null, maxValue = null) => {
    return Validator.validateInteger('filter', fieldName, fieldValue, false, minValue, maxValue)
  },

  validateIntegerField: (fieldName, fieldValue, required = true, minValue = null, maxValue = null) => {
    return Validator.validateInteger('field', fieldName, fieldValue, required, minValue, maxValue)
  },

  validateInteger: (fieldLabel, fieldName, fieldValue, required = true, minValue = null, maxValue = null) => {
    if (required && fieldValue === null) {
      Validator.addError(`The ${fieldLabel} "${fieldName}" was not sent`)
    }
    
    if (fieldValue !== null) {
      const intValue = Number.parseInt(fieldValue)

      if (Number.isInteger(intValue)) {
        if (minValue !== null && fieldValue < minValue) {
          Validator.addError(`The value sent in the "${fieldName}" ${fieldLabel} must be greater than or equal to ${minValue}`)
        }
        
        if (maxValue !== null && fieldValue > maxValue) {
          Validator.addError(`The value sent in the "${fieldName}" ${fieldLabel} must be less than or equal to ${maxValue}`)
        }
      } else {
          Validator.addError(`The value sent in the "${fieldName}" ${fieldLabel} must be an integer number`)
      }
    }

    return fieldValue
  },

  validateEnumFilter: (fieldName, fieldValue, enumList, trim = false, toLowerCase = false) => {
    return Validator.validateEnum('filter', fieldName, fieldValue, enumList, false, trim, toLowerCase)  
  },
  
  validateEnumField: (fieldName, fieldValue, enumList, required = true, trim = false, toLowerCase = false) => {
    return Validator.validateEnum('field', fieldName, fieldValue, enumList, required, trim, toLowerCase)  
  },

  validateEnum: (fieldLabel, fieldName, fieldValue, enumList, required = true, trim = false, toLowerCase = false) => {
    if (required && fieldValue === null) {
      Validator.addError(`The ${fieldLabel} "${fieldName}" was not sent`)
    }

    if (trim) {
      fieldValue = fieldValue.trim()
    }

    if (toLowerCase) {
      fieldValue = fieldValue.toLowerCase()
    }
    
    if (!enumList.includes(fieldValue)) {
      Validator.addError(`The ${fieldLabel} value "${fieldName}" must be in list: [${enumList.join(', ')}]`)
    }            
    
    return fieldValue
  },

  validateUuidFilter: (fieldName, fieldValue) => {
    return Validator.validateUuid('filter',fieldName, fieldValue, false)
  },

  validateUuidField: (fieldName, fieldValue, required = true) => {
    return Validator.validateUuid('field',fieldName, fieldValue, required)
  },
  
  validateUuid: (fieldLabel, fieldName, fieldValue, required = true) => {
    if (required && fieldValue === null) {
      Validator.addError(`The ${fieldLabel} "${fieldName}" was not sent`)
    }      
    
    if (fieldValue !== null && !uuid.validate(fieldValue)) {
      Validator.addError(`The ${fieldLabel} value "${fieldName}" is not a valid UUID value`)
    }  

    return fieldValue
  },

  validateBrazilianZipCodeField: (fieldName, fieldValue, required) => {
    return Validator.validateByRegex('field', fieldName, fieldValue, /^([\d]{2})\.*([\d]{3})-*([\d]{3})$/, required)
  },

  validateByRegex: (fieldLabel, fieldName, fieldValue, regex, required = true) => {
    if (required && fieldValue === null) {
      Validator.addError(`The ${fieldLabel} "${fieldName}" was not sent`)
    }
        
    if (fieldValue !== null) {
      const matches = fieldValue.match(regex)

      const valid = matches !== null

      if (!valid) {
        Validator.addError(`The value sent in the "${fieldName}" ${fieldLabel} is invalid`)
      }
    }

    return fieldValue
  },
}

module.exports = Validator