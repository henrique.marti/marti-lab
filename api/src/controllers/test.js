const controllerTools = require('./controllerTools')
const laboratoryModel = require('../models/laboratory')
const testModel = require('../models/test')
const validator = require('./validator')

const fields = ['id', 'name', 'type', 'status']
const validStatus = ['all','active', 'inactive']
const validTypes = ['all','clinical_analysis', 'image']

const include = [
  {
    model: laboratoryModel,
    as: 'laboratories',
    attributes: ['id','name', 'status'],
    through: { attributes: [] }
  }
]

const testController = {
  getAll: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, null, getFilters)

      const data = await testModel.findAll({
        where,
        attributes: fields
      })

      res.status(200).json(data)
    } catch(e) {
      next(e)
    }
  },

  getById: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'ID' })
      const testEntity = await controllerTools.loadData(testModel, fields, where)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${where.id}`
        })
        return
      }

      res.status(200).json(testEntity)
    } catch(e) {
      next(e)
    }
  },

  searchLaboratories: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, null, getFilterSearch)
      const testEntity = await controllerTools.loadData(testModel, fields, where, include)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${where.id}`
        })
        return
      }

      return res.status(200).json(testEntity.laboratories)
    } catch(e) {
      next(e)
    }
  },

  create: async (req, res, next) => {
    try {
      const createFields = await getCreateFields(req, validator)

      const createdTest = await testModel.create(createFields)

      const testEntity = await controllerTools.loadData(testModel, fields, { id: createdTest.id })

      res.status(201).json(testEntity)
    } catch(e) {
        next(e)
    }
  },

  update: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'ID' })
      let testEntity = await controllerTools.loadData(testModel, fields, where)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${where.id}`
        })
        return
      }

      testEntity = await getUpdateFields(req, validator, testEntity)

      await testEntity.save()

      testEntity = await controllerTools.loadData(testModel, fields, where)

      return res.status(200).json(testEntity)
    } catch(e) {
      next(e)
    }
  },

  delete: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'ID' })
      let testEntity = await controllerTools.loadData(testModel, fields, where, include)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${where.id}`
        })
        return
      }

      await testEntity.destroy()
      res.status(204).end()
    } catch(e) {
      next(e)
    }
  },

  getLaboratories: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'test ID' })
      const testEntity = await controllerTools.loadData(testModel, fields, where, include)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${where.id}`
        })
        return
      }

      return res.status(200).json(testEntity.laboratories)
    } catch(e) {
      next(e)
    }
  },

  addLaboratiry: async (req, res, next) => {
    try {
      const testWhere = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'test ID' })
      let testEntity = await controllerTools.loadData(testModel, fields, testWhere, include)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${testWhere.id}`
        })
        return
      }

      if (testEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Test ID ${testWhere.id} is inactive`
        })
        return
      }

      const laboratoryWhere = await controllerTools.getWhere(req, { field: 'id', param: 'laboratoryId', label: 'laboratory ID' })
      const laboratoryEntity = await controllerTools.loadData(laboratoryModel, null, laboratoryWhere)

      if (!laboratoryEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${laboratoryWhere.id}`
        })
        return
      }

      if (laboratoryEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Laboratory ID ${laboratoryWhere.id} is inactive`
        })
        return
      }

      const laboratories = testEntity.laboratories.map(laboratory => {
        return laboratory.id
      })

      if (laboratories.includes(laboratoryWhere.id)) {
        res.status(400).json({
          erro: `Laboratory id ${laboratoryWhere.id} already belongs to test id ${testWhere.id}`
        })
        return
      }

      laboratories.push(laboratoryWhere.id)

      testEntity.setLaboratories(laboratories)

      return res.status(204).end()
    } catch(e) {
      next(e)
    }
  },

  removeLaboratiry: async (req, res, next) => {
    try {
      const testWhere = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'test ID' })
      let testEntity = await controllerTools.loadData(testModel, fields, testWhere, include)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${testId}`
        })
        return
      }

      if (testEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Test ID ${testWhere.id} is inactive`
        })
        return
      }

      const laboratoryWhere = await controllerTools.getWhere(req, { field: 'id', param: 'laboratoryId', label: 'laboratory ID' })
      const laboratoryEntity = await controllerTools.loadData(laboratoryModel, null, laboratoryWhere)

      if (!laboratoryEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${laboratoryWhere.id}`
        })
        return
      }

      if (laboratoryEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Laboratory ID ${laboratoryWhere.id} is inactive`
        })
        return
      }

      const laboratories = testEntity.laboratories.map(laboratory => {
        return laboratory.id
      })

      if (!laboratories.includes(laboratoryWhere.id)) {
        res.status(400).json({
          erro: `Laboratory id ${laboratoryWhere.id} does not belongs to test id ${testWhere.id}`
        })
        return
      }

      laboratories.pop(laboratoryWhere.id)

      testEntity.setLaboratories(laboratories)

      return res.status(204).end()
    } catch(e) {
      next(e)
    }
  },
}

const getFilters = async (req, conditions) => {
  validator.clearExistingErrors()

  let { id = null, name = null, type = 'all', status = 'active' } = req.query

  id = validator.validateUuidFilter('id', id)
  if (id !== null) {
      conditions.id = id
  }

  name = validator.validateTextFilter('name', name)
  if (name !== null) {
      conditions.name = name
  }

  status = validator.validateEnumFilter('status', status, validStatus, true, true)
  if (status != 'all') {
      conditions.status = status
  }

  type = validator.validateEnumFilter('type', type, validTypes, true, true)
  if (type != 'all') {
      conditions.type = type
  }

  validator.throwExistingErrors()

  return conditions
}

const getFilterSearch = async (req, conditions) => {
  validator.clearExistingErrors()

  let { id = null, name = null } = req.query

  id = validator.validateUuidFilter('id', id)
  if (id !== null) {
      conditions.id = id
  }

  name = validator.validateTextFilter('name', name)
  if (name !== null) {
      conditions.name = name
  }

  if (id == null && name == null) {
    validator.addError('You must submit "id" or "name" to search')
  }

  validator.throwExistingErrors()

  return conditions
}

const getCreateFields = async (req, validator) => {
  validator.clearExistingErrors()

  let { name = null, type = null, status = 'active' } = req.body

  name = validator.validateTextField('name', name)
  status = validator.validateEnumField('status', status, validStatus, true, true, true)
  type = validator.validateEnumField('type', type, validTypes, true, true, true)

  validator.throwExistingErrors()

  return {
      name,
      type,
      status,
  }
}

const getUpdateFields = async (req, validator, testEntity) => {
  validator.clearExistingErrors()

  let { name = null, type = null, status = null } = req.body

  name = name || testEntity.name
  type = type || testEntity.type
  status = status || testEntity.status

  testEntity.name = validator.validateTextField('name', name, false)
  testEntity.status = validator.validateEnumField('status', status, validStatus, false, true, true)
  testEntity.type = validator.validateEnumField('type', type, validTypes, false, true, true)

  validator.throwExistingErrors()

  return testEntity
}

module.exports = testController
