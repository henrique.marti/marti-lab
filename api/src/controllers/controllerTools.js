const uuid = require('uuid')

const controllerTools = {
  getWhere: async (req, uuidField = null, getFilters = null) => {
    let where = {}

    if (uuidField) {
      where[uuidField.field] = await controllerTools.getUuidField(req, uuidField)
    }

    if (getFilters !== null) {
      where = await getFilters(req, where)
    }

    return where
  },

  getUuidField: async (req, uuidField) => {
    const fieldValue = req.params[uuidField.param]

    if (!uuid.validate(fieldValue)) {
      throw TypeError(`The sent ${uuidField.label} is not a valid UUID value`)
    }

    return fieldValue
  },

  loadData: async (model, fields, where, include = undefined) => {
    const data = await model.findOne({
      where,
      attributes: fields,
      include,
    })

    return data
  }

}

module.exports = controllerTools
