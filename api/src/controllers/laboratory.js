const addressModel = require('../models/address')
const controllerTools = require('./controllerTools')
const laboratoryModel = require('../models/laboratory')
const testModel = require('../models/test')
const validator = require('./validator')

const fields = ['id', 'name', 'status']
const validStatus = ['all','active', 'inactive']
const validStates = ['AC','AL','AP','AM','BA','CE','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO','DF']

const include = [
  {
    model: addressModel,
    as: 'address',
    attributes: ['id', 'address', 'number', 'district', 'city', 'state'],
  },
]

const testInclude = [
  {
    model: testModel,
    as: 'tests',
    attributes: ['id', 'name', 'type', 'status'],
    through: { attributes: [] }
  },
]

const laboratoryController = {
  getAll: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, null, getFilters)

      const data = await laboratoryModel.findAll({
        where,
        attributes: fields,
        include
      })

      res.status(200).json(data)
    } catch(e) {
      next(e)
    }
  },

  getById: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'ID' })
      const laboratotyEntity = await controllerTools.loadData(laboratoryModel, fields, where, include)

      if (!laboratotyEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${where.id}`
        })
        return
      }

      res.status(200).json(laboratotyEntity)
    } catch(e) {
      next(e)
    }
  },

  create: async (req, res, next) => {
    try {
      const createFields = await getCreateFields(req, validator)

      const createdLaboratory = await laboratoryModel.create(createFields.laboratory)

      createFields.address.laboratory_id = createdLaboratory.id
      await addressModel.create(createFields.address)

      const laboratotyEntity = await controllerTools.loadData(laboratoryModel, fields, { id: createdLaboratory.id }, include)

      res.status(201).json(laboratotyEntity)
    } catch(e) {
        next(e)
    }
  },

  update: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'ID' })
      let laboratotyEntity = await controllerTools.loadData(laboratoryModel, fields, where, include)

      if (!laboratotyEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${where.id}`
        })
        return
      }

      laboratotyEntity = await getUpdateFields(req, validator, laboratotyEntity)

      await laboratotyEntity.save()
      await laboratotyEntity.address.save()

      laboratotyEntity = await controllerTools.loadData(laboratoryModel, fields, where, include)

      return res.status(200).json(laboratotyEntity)
    } catch(e) {
      next(e)
    }
  },

  delete: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'ID' })
      let laboratotyEntity = await controllerTools.loadData(laboratoryModel, fields, where, include)

      if (!laboratotyEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${where.id}`
        })
        return
      }

      await laboratotyEntity.destroy()
      res.status(204).end()
    } catch(e) {
      next(e)
    }
  },

  getTests: async (req, res, next) => {
    try {
      const where = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'laboratory ID' })
      let laboratoryEntity = await controllerTools.loadData(laboratoryModel, fields, where, testInclude)

      if (!laboratoryEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${where.id}`
        })
        return
      }

      return res.status(200).json(laboratoryEntity.tests)
    } catch(e) {
      next(e)
    }
  },

  addTest: async (req, res, next) => {
    try {
      const laboratoryWhere = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'laboratory ID' })
      let laboratoryEntity = await controllerTools.loadData(laboratoryModel, fields, laboratoryWhere, testInclude)

      if (!laboratoryEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${laboratoryWhere.id}`
        })
          return
      }

      if (laboratoryEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Laboratory ID ${laboratoryWhere.id} is inactive`
        })
        return
      }

      const testWhere = await controllerTools.getWhere(req, { field: 'id', param: 'testId', label: 'test ID' })
      const testEntity = await controllerTools.loadData(testModel, null, testWhere)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${testWhere.id}`
        })
        return
      }

      if (testEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Test ID ${testWhere.id} is inactive`
        })
        return
      }

      const tests = laboratoryEntity.tests.map(test => {
        return test.id
      })

      if (tests.includes(testWhere.id)) {
        res.status(400).json({
          erro: `Test id ${testWhere.id} already belongs to laboratory id ${laboratoryWhere.id}`
        })
        return
      }

      tests.push(testWhere.id)

      laboratoryEntity.setTests(tests)

      return res.status(204).end()
    } catch(e) {
      next(e)
    }
  },

  removeTest: async (req, res, next) => {
    try {
      const laboratoryWhere = await controllerTools.getWhere(req, { field: 'id', param: 'id', label: 'laboratory ID' })
      let laboratoryEntity = await controllerTools.loadData(laboratoryModel, fields, laboratoryWhere, testInclude)

      if (!laboratoryEntity) {
        res.status(404).json({
          erro: `Could not find a laboratory with id ${laboratoryWhere.id}`
        })
        return
      }

      if (laboratoryEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Laboratory ID ${laboratoryWhere.id} is inactive`
        })
        return
      }

      const testWhere = await controllerTools.getWhere(req, { field: 'id', param: 'testId', label: 'test ID' })
      const testEntity = await controllerTools.loadData(testModel, null, testWhere)

      if (!testEntity) {
        res.status(404).json({
          erro: `Could not find a test with id ${testWhere.id}`
        })
        return
      }

      if (testEntity.status == 'inactive') {
        res.status(400).json({
          erro: `Test ID ${testWhere.id} is inactive`
        })
        return
      }

      const tests = laboratoryEntity.tests.map(test => {
        return test.id
      })

      if (!tests.includes(testWhere.id)) {
        res.status(400).json({
          erro: `Test id ${testWhere.id} does not belongs to laboratory id ${laboratoryWhere.id}`
        })
        return
      }

      tests.pop(testWhere.id)

      laboratoryEntity.setTests(tests)

      return res.status(204).end()
    } catch(e) {
      next(e)
    }
  },
}

const getFilters = async (req, conditions) => {
  validator.clearExistingErrors()

  let { id = null, name = null, status = 'active' } = req.query

  id = validator.validateUuidFilter('id', id)
  if (id !== null) {
      conditions.id = id
  }

  name = validator.validateTextFilter('name', name)
  if (name !== null) {
      conditions.name = name
  }

  status = validator.validateEnumFilter('status', status, validStatus, true, true)
  if (status != 'all') {
      conditions.status = status
  }

  validator.throwExistingErrors()

  return conditions
}

const getCreateFields = async (req, validator) => {
  validator.clearExistingErrors()

  let { name = null, status = 'active', address } = req.body

  name = validator.validateTextField('name', name)
  status = validator.validateEnumField('status', status, validStatus, true, true, true)
  address = await getCreateAddressFields(address, validator)

  validator.throwExistingErrors()

  return {
    laboratory: {
      name,
      status,
    },
    address,
  }
}

const getCreateAddressFields = async (addressData, validator) => {
  if (addressData == null) {
    validator.addError(`The address field was not sent`)
    return null
  }

  let { address, number, complement = null, district, city, state } = addressData

  address = validator.validateTextField('address', address)
  number = validator.validateIntegerField('number', number)
  complement = validator.validateTextField('complement', complement, false)
  district = validator.validateTextField('district', district)
  city = validator.validateTextField('city', city)
  state = validator.validateEnumField('state', state, validStates, true, true)

  return {
      address,
      number,
      complement,
      district,
      city,
      state,
  }
}

const getUpdateFields = async (req, validator, laboratoryEntity) => {
  validator.clearExistingErrors()

  let { name = null, status = null, address = null } = req.body

  name = name || laboratoryEntity.name
  status = status || laboratoryEntity.status

  laboratoryEntity.name = validator.validateTextField('name', name, false)
  laboratoryEntity.status = validator.validateEnumField('status', status, validStatus, false, true, true)
  laboratoryEntity.address = await getUpdateAddressFields(address, validator, laboratoryEntity.address)

  validator.throwExistingErrors()

  return laboratoryEntity
}

const getUpdateAddressFields = async (addressData, validator, addressEntity) => {
  let {
    address = addressEntity.address,
    number = addressEntity.number,
    complement = addressEntity.complement,
    district = addressEntity.district,
    city = addressEntity.city,
    state = addressEntity.state
  } = addressData

  addressEntity.address = validator.validateTextField('address', address)
  addressEntity.number = validator.validateIntegerField('number', number)
  addressEntity.complement = validator.validateTextField('complement', complement, false)
  addressEntity.district = validator.validateTextField('district', district)
  addressEntity.city = validator.validateTextField('city', city)
  addressEntity.state = validator.validateEnumField('state', state, validStates, true, true)

  return addressEntity
}

module.exports = laboratoryController
