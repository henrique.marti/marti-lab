const documentationFile = require('../doc/swagger_output.json')
const express = require('express')
const router = express.Router()
const swaggerUi = require('swagger-ui-express')
const v1 = require('./v1')

router.get('/', (req, res) => {
    res.send(`<html><head><title>Marti Lab</title></head><body><h1>Marti Lab</h1><p>Raiz</p></body></html>`)
})

router.use('/doc', swaggerUi.serve, swaggerUi.setup(documentationFile))

router.use('/v1', v1)

router.use((err, req, res, next) => {
  let errors = err.message;

  errors = errors.split('<||>')

  res.status(400).json({ errors })
})

module.exports = router
