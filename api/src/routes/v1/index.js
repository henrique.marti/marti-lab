const express = require('express')
const router = express.Router()

const laboratoryRoutes = require('./laboratory')
const testRoutes = require('./test')

router.get('/', (req, res) => {
    res.send(`<html><head><title>Marti Lab</title></head><body><h1>Marti Lab</h1><p>Versão 1</p></body></html>`)
})

router.use('/laboratories', laboratoryRoutes)
router.use('/tests', testRoutes)


module.exports = router
