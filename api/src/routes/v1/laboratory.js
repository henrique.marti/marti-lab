const controller = require('../../controllers/laboratory')
const express = require('express')
const router = express.Router()

router.get('/', controller.getAll)

router.get('/:id', controller.getById)

router.post('/', controller.create)

router.put('/:id', controller.update)
router.patch('/:id', controller.update)

router.delete('/:id', controller.delete)

router.get('/:id/tests', controller.getTests)

router.post('/:id/tests/:testId', controller.addTest)

router.delete('/:id/tests/:testId', controller.removeTest)

module.exports = router
