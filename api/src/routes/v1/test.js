const controller = require('../../controllers/test')
const express = require('express')
const router = express.Router()

router.get('/', controller.getAll)

router.get('/search-laboratories', controller.searchLaboratories)

router.get('/:id', controller.getById)

router.post('/', controller.create)

router.put('/:id', controller.update)
router.patch('/:id', controller.update)

router.delete('/:id', controller.delete)

router.get('/:id/laboratories', controller.getLaboratories)

router.post('/:id/laboratories/:laboratoryId', controller.addLaboratiry)

router.delete('/:id/laboratories/:laboratoryId', controller.removeLaboratiry)

module.exports = router
