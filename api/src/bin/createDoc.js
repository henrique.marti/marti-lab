const swaggerAutogen = require('swagger-autogen')()

const documentationOutput = 'src/doc/swagger_output.json'
const routesFile = ['src/routes/index.js']

swaggerAutogen(documentationOutput, routesFile)
