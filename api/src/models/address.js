const { Model, DataTypes } = require('sequelize')
const uuid = require('uuid')

class Address extends Model {
  static init(sequelize) {
    super.init({
        address: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'address',
        },        
        number: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'number',
        },        
        complement: {
            type: DataTypes.STRING,
            allowNull: true,
            columnName: 'complement',
        },
        district: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'district'
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'city',
        },
        state: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'state',
        }
    }, {
      sequelize,
      modelName: 'Address',
      paranoid: true,
      tableName: 'addresses',
      hooks: {
        beforeCreate: (reg, options) => {
          reg.id = uuid.v4()
        },
      },
    })
  }

  static associate(models) {
    this.belongsTo(models.Laboratory, { foreignKey:'laboratory_id', as:'empresa' })
  }
}

module.exports = Address
