const { Model, DataTypes } = require('sequelize')
const uuid = require('uuid')

class Laboratory extends Model {
  static init(sequelize) {
    super.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'name',
        },
        status: {
            type: DataTypes.ENUM('active','inactive'),
            allowNull: false,
            columnName: 'status',
        },
    }, {
      sequelize,
      modelName: 'Laboratory',
      paranoid: true,
      tableName: 'laboratories',
      hooks: {
        beforeCreate: (reg, options) => {
          reg.id = uuid.v4()
        },
      },
    })
  }

  static associate(models) {
    this.hasOne(models.Address, { foreignKey:'laboratory_id', as:'address' })
    this.belongsToMany(models.Test, { through: 'laboratories_tests', foreignKey:'laboratory_id', as: 'tests' })
  }
}

module.exports = Laboratory
