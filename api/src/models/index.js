const Sequelize = require('sequelize')
const dbConfig = require('../config/index')

const connection = new Sequelize(dbConfig)

const Address = require('./address')
const Laboratory = require('./laboratory')
const Test = require('../models/test')

Address.init(connection)
Laboratory.init(connection)
Test.init(connection)

Address.associate(connection.models)
Laboratory.associate(connection.models)
Test.associate(connection.models)

module.exports = connection
