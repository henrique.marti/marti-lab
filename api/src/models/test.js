const { Model, DataTypes } = require('sequelize')
const uuid = require('uuid')

class Test extends Model {
  static init(sequelize) {
    super.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            columnName: 'name',
        },
        type: {
            type: DataTypes.ENUM('clinical_analysis','image'),
            allowNull: false,
            columnName: 'type',
        },
        status: {
            type: DataTypes.ENUM('Disponível', 'Alocado', 'Inativo'),
            allowNull: false,
            columnName: 'status',
        },        
    }, {
      sequelize,
      modelName: 'Test',
      paranoid: true,
      tableName: 'tests',
      hooks: {
        beforeCreate: (reg, options) => {
          reg.id = uuid.v4()
        },
      },
    })
  }

  static associate(models) {
    this.belongsToMany(models.Laboratory, { through: 'laboratories_tests', foreignKey:'test_id', as: 'laboratories' })
  }
}

module.exports = Test
