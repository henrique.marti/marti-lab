# Marti Lab

Exemplo de API Rest para gerenciar o cadastro de laboratórios e exames, além do relacionamento entre eles.

## Dependências
 - Docker
 - Docker Compose

## Começando
**1 - Clone o repositório**
```
git clone https://gitlab.com/henrique.marti/marti-lab
```
_**Importante:** Após clonar o repositório entre no seu diretório, o diretório padrão é marti-lab_

**2 - Inicie os containers com Docker Compose**
```
docker-compose up -d
```

**3 - Instale as dependências do projeto e rode as migrations no banco de dados**
```
./prepare_environment.sh
```

**4 - Abra seu navegador e acesse a URL da documentação da API**
```
http://localhost/doc
```

## Postman Collection

Para faciliar os testes, no diretório raiz do projeto existe uma collection do Postman com todos os end-points da API. Para usá-la basta ter o Postman instalado, abri-lo e importar o arquivo `Marti Lab.postman_collection.json`. 

## Tecnologias
 - Docker
 - Docker Compose
 - Postgres
 - Javascript
 